+++
date = "2016-06-20T15:47:24+04:00"
tags=["Ruby", "OSX", "openssl", "rvm", "problem", "solution"]
title = "Error installing ruby 2.3 on osx"
categories = ["Ruby", "OSX"]
+++
Resolve problem with Ruby 2.3 on OS X
<!--more-->
Problem:
```
% rvm install 2.3
.....
.....
.....
ruby-2.3.1 - #configuring - please wait
ruby-2.3.1 - #post-configuration - please wait
ruby-2.3.1 - #compiling - please wait
Error running '__rvm_make -j 1',
showing last 15 lines of /Users/andrewdruzinin/.rvm/log/1466449338_ruby-2.3.1/make.log
^~~~~~~~~~~~~~~~~~~~~~~~~~~~~
ossl_ssl.c:18:35: note: expanded from macro 'numberof'
#define numberof(ary) (int)(sizeof(ary)/sizeof((ary)[0]))
^~~~~
ossl_ssl.c:2267:21: error: invalid application of 'sizeof' to an incomplete type 'const struct (anonymous struct at ossl_ssl.c:85:14) []'
for (i = 0; i < numberof(ossl_ssl_method_tab); i++) {
^~~~~~~~~~~~~~~~~~~~~~~~~~~~~
ossl_ssl.c:18:35: note: expanded from macro 'numberof'
#define numberof(ary) (int)(sizeof(ary)/sizeof((ary)[0]))
^~~~~
6 warnings and 9 errors generated.
make[2]: *** [ossl_ssl.o] Error 1
make[1]: *** [ext/openssl/all] Error 2
make: *** [build-ext] Error 2
+__rvm_make:0> return 2
There has been an error while running make. Halting the installation.
```
Solution:
```
brew uninstall ->force openssl
brew install openssl
brew link ->overwrite openssl ->force
rvm install 2.3.1
```
